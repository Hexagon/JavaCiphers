package javaciphers_de;
import java.util.Scanner; //Gibt die Möglichkeit, eigene Eingaben zu tätigen

public class Skytale {

	public static void main(String[] args) { 
		try (Scanner input = new Scanner(System.in)) { //Gibt die Möglichkeit, eigene Eingaben über "input" zu verwerten
			System.out.println("Text eingeben"); //Fordert zur Eingabe des (Klar-)Textes auf
			String text = input.nextLine(); //Erstellt eine Variable "text", die als Wert die Eingabe annimmt
			System.out.println("Schlüssel eingeben"); //Fordert zur Eingabe des Schlüssels auf
			int schluessel = input.nextInt(); //Erstellt eine Variable "schluessel", die als Wert die Eingabe annimmt
			int laenge = text.length(); //Zählt die Buchstaben der Eingabe
			System.out.println("Verschlüsseln (1) oder Entschlüsseln (2)?"); //Fordert zur Eingabe von 1 oder 2 und somit der Wahl des Programms auf
			int auswahl = input.nextInt(); //Erstellt eine Variable "auswahl", die als Wert die Eingabe annimmt
			System.out.println("-------------------------------"); //Trenner zu Dekorationszwecken
			
			if(auswahl == 1) {
				System.out.println("Verschlüsselter Text:"); //Kündigt den verschlüsselten Text an
				int i = 0; //Erstellt die Variable i mit dem Wert 0
				int j = 0; //Erstellt die Variable j mit dem Wert 0
				while(j < schluessel) {
				   while(i < laenge) {
				      char ausgabe = text.charAt(i);
				      i = i + schluessel;
				      System.out.print(ausgabe);
				   }
				   j = j + 1;
				   i = j;
				}
			}else {
				if(auswahl == 2) {
					
						System.out.println("Entschlüsselter Text:"); //Kündigt den verschlüsselten Text an
						int entschluessel = laenge / schluessel;
						int modulo = laenge % schluessel;
						if(modulo > 0) {
							entschluessel++;
						}	
						char fueller = 'X';					
						if(modulo > 0) {
							text += fueller;							
						}
						int i = 0; //Erstellt die Variable i mit dem Wert 0
						int j = 0; //Erstellt die Variable j mit dem Wert 0
						while(j < entschluessel) {
						   while(i < laenge) {
						      char ausgabe = text.charAt(i);
						      i = i + entschluessel;
						      System.out.print(ausgabe);
						   }
						   j = j + 1;
						   i = j;
						}
					
				}else {
					System.out.println("Ungültig!");
				}
			}

		}
	}

}