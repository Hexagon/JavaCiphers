package javaciphers_en;

import java.util.Scanner; //Enables the option to enter own content for variables

public class Caesar {

	public static void main(String[] args) {
		try (Scanner input = new Scanner(System.in)) { //Gives the ability to use own input via "input"
			System.out.println("Enter text (Only letters permitted!)"); //Asks to enter secret or plain text
			String text = input.nextLine(); //Creates the variable "text" which is set to text entered by the user
			int laenge = text.length(); //Counts the characters of "text"
			System.out.println("Enter key (>0 for encryption, <0 for decryption)"); //Asks to enter the key
			int key = input.nextInt(); //Creates the variable "key" which is set to the entered key
			System.out.println(""); //Empty line for design purposes
			System.out.println("-------------------------------"); //Separator for design purposes
			System.out.println(""); //Empty line for design purposes
			System.out.println("Output:"); //Marks the beginning of the output
			while(key > 26) { //Subtracts 26 from a positive key above 26 until it is less than or equal to 26, as anything above that is just a repetition.
			   key = key - 26;
			}
			while(key < 0) { //Adds 26 to a negative key until it is positive and the text can thus be decrypted by shifting.
			   key = key + 26;
			}
			int i = 0; //Creates the variable i with the value 0
			int j = 0; //Creates the variable j with the value 0
			int capitalletter = 0; //Creates the variable capitalletter with the value 0
			while(i < laenge) { //REpeats the following code until all letters are shifted
			   char c = text.charAt(i); //Creates the variable "c" which contains the letter at the position "i"
			   j = (int) c; //"j" is set to the ASCII equivalent of "c"
			   if(j >= 65 && j <= 90) { //Checks via the ASCII value whether "c" is a capital letter
			      capitalletter = 1; //If yes, "capitalletter" is set to 1
			   }else{ //If not, "capitalletter" is set to 0
			      capitalletter = 0;
			   }
			   j = j + key; //The key entered before is added to the ASCII value of "c"
			   if(capitalletter == 1) { //For capital letters:
			      if(j <= 90) { //Checks if the new value of "j" is less than or equal to 90 (in the ASCII range of capital letters)
			         c = (char) j; //"c" is set to the letter with the ASCII value "j"
			         System.out.print(c); //The shifted letter "c" is printed
			      }else{ //If "j" is bigger than 90 (no capital letter):
			         j = j - 26; //26 is subtracted from the value of "j" to simulate a shift within the alphabet
			         c = (char) j; //"c" is set to the letter with the ASCII value "j"
			         System.out.print(c); //The shifted letter "c" is printed
			      }
			   }else{ //If "j" is no capital letter:
			      if(j <= 122) { //Checks if the new value of "j" is less than or equal to 122 (in the ASCII range of lower case letters)
			         if(j >= 97) { //If yes: Checks if the new value of "j" is also bigger than or equal to 97 (in the ASCII range of lower case letters)
			            c = (char) j; //If yes: "c" is set to the letter with the ASCII value "j"
			            System.out.print(c); //The shifted letter "c" is printed
			         }else{ //If the value of "j" is too small (smaller than 97):
			            j = j + 26; //26 is added to the value of "j" to simulate a shift within the alphabet
			            c = (char) j; //"c" is set to the letter with the ASCII value "j"
			            System.out.print(c); //The shifted letter "c" is printed
			         }
			      }else{ //If "j" is bigger than 122:
			         j = j - 26; //26 is subtracted from the value of "j" to simulate a shift within the alphabet
			         c = (char) j; //"c" is set to the letter with the ASCII value "j"
			         System.out.print(c); //The shifted letter "c" is printed
			      }

			   }
			   i = i + 1; //1 is added to the position counter
			}
		}
	}

}
