# JavaCiphers

Comments and variables in English (EN) and German (DE).

## EN:

### Currently available:

- Caesar cipher (https://en.wikipedia.org/wiki/Caesar_cipher)
- Rail fence cipher (key = 2) (https://en.wikipedia.org/wiki/Rail_fence_cipher)

### Planned:

- Skytale (https://en.wikipedia.org/wiki/Scytale)
- Vigenère cipher (https://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher)

---

## DE:

### Aktuell verfügbar:

- Caesar-Verschlüsselung (https://de.wikipedia.org/wiki/Caesar-Verschl%C3%BCsselung)
- Gartenzaun-Verschlüsselung (mit dem Schlüssel 2) (http://casa.it.bzz.ch/wiki/thema/ictgrundlagen/skript/gartenzaun)

### In Planung:

- Skytale (https://de.wikipedia.org/wiki/Skytale)
- Vigenère-Chiffre (https://de.wikipedia.org/wiki/Vigen%C3%A8re-Chiffre)