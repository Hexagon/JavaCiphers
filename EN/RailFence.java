package javaciphers_en;


import java.util.Scanner; //Enables the option to enter own content for variables

public class RailFence {

	public static void main(String[] args) {

		try (Scanner input = new Scanner(System.in)) { //Gives the ability to use own input via "input"
			System.out.println("Enter text"); //Asks to enter (secret or plain) text
			String text = input.nextLine(); //Creates the variable "text" which is set to text entered by the user
			int length = text.length(); //Counts the letters of "text"
			int i = 0; //Creates the variable "i" (for "odd" letters) with the value 0
			int ii = 0; //Creates the variable "ii" (for "even" letters) with the value 0
			System.out.println("Encrypt (1) oder Decrypt (2)?"); //Asks to enter either 1 or 2 and choose the operation mode
			int choice = input.nextInt(); //Creates the variable "choice" set to the entered number
			System.out.println("-------------------------------"); //Separator for design purposes
			if(choice == 1) { //If "Encrypt" was chosen:
				System.out.println("Encrypted text:"); //Announces the encrypted text
				char c; //Creates the variable "c"
				while(i < length) { //Until the end of the word is reached:
					c = text.charAt(i); //"c" is set to the letter at the position "i"
					System.out.print(c); //"c" is printed
					i = i + 2; //Counts on to the next "odd" letter
				}
				ii = 1; //"ii" is set to 1 (for the first "even" letter)
				while(ii < length) { //Until the end of the word is reached:
					c = text.charAt(ii); //"c" is set to the letter at the position "ii"
					System.out.print(c); //"c" is printed
					ii = ii + 2; //Counts on to the next "even" letter
				}
			}else { //If "Encrypt" was not chosen:
				if(choice == 2) { //Checks if "Decrypt" was chosen. If yes:
					System.out.println("Decrypted text:"); //Announces the decrypted text
					int j = 0; //Creates the variable "j" (for the position of the current letter) with the value 0
					int lengthpartone = length / 2; //Sets the length for part 1 (the "odd" letters in the plain text) to the half of the length of "text"
					int lengthodd = length % 2; //Checks if there is a rest after dividing "length" by 2 (so the text has an odd number of letters)
					if(lengthodd == 1) { //if the text has an odd number of letters:
					   lengthpartone = lengthpartone + 1; //The length of part one has to be longer by 1 letter because in an "odd" text there is one more "odd" letter
					}
					ii = lengthpartone; //Sets the counter for "even" letters to the length of part one because the "even" letters begin afterwards
					char plaintext; //Creates the character variable "plaintext"
					while(j < length) { //As long as not all letters have been printed:
					   plaintext = text.charAt(i); //"plaintext" is set to the first (resp. next) "odd" letter (at the position "i")
					   System.out.print(plaintext); //"plaintext" is printed
					   i = i + 1; //Counts to the next "odd" letter
					   j = j + 1; //The character counter counts up by 1
					   if(ii < length) { //If there still is an "even" letter:
					   plaintext = text.charAt(ii); //"plaintext" is set to the first (resp. next) "even" letter (at the position "ii")
					   System.out.print(plaintext); //"plaintext" is printed
					   ii = ii + 1; //Counts to the next "even" letter
					   j = j + 1; //The character counter counts up by 1
					   }
					}
				}else { //If neither "Encrypt" (1) nor "Decrypt" (2) were chosen:
					System.out.println("Invalid!"); //The error "Invalid!" is printed
				}
			}
		}

	}

}
